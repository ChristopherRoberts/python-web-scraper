import pandas as pd
import httplib2
from bs4 import BeautifulSoup, SoupStrainer
from lxml import html
import requests
import os
import json

WEB_PAGE = "https://orgsync.com/390/community/opportunities"
EVENT_PAGE = ""
BASE_URL = "https://orgsync.com/"

http = httplib2.Http()
status, response = http.request(WEB_PAGE)
soup = BeautifulSoup(response, "html.parser")


dictofevents = {}
singleeventdict = {}
temp = []
writein = []
text = ""
field = ""
c = 0
for link in soup.find_all('a'):
    if "popup" not in link.get('href'):
        continue
    EVENT_PAGE = BASE_URL + link.get('href').replace('display_event_popup', '')
    singleeventdict['url'] = EVENT_PAGE
    tempresponse = requests.get(EVENT_PAGE)
    event = BeautifulSoup(tempresponse.content, "html.parser")
    temp = event.find_all('div', {'class': "align"})
    singleeventdict['title'] = event.find('h2').text
    blah = event.find('div', {'class': "subtle-text left"}).text
    blah = blah.replace(" ", "")
    blah = blah.replace("\n", "")
    blah = blah.replace(",", ", ")
    singleeventdict['time'] = blah
    blah = event.find('div', {'class': "align"}).text.replace(singleeventdict['time'], '')
    blah = blah.replace(" ", "")
    blah = blah.replace("\n", "")
    blah = blah.replace(",", ", ")
    singleeventdict['date'] = blah

    writein = event.find_all('p')

    for each in writein:
        if each.find('strong'):
            field = each.find('strong').text
            text = each.text.replace(field, '')
            field = field.replace(":", '')
            field = field.replace("\xa0", "")
            singleeventdict[field] = text

    dictofevents[c] = dict(singleeventdict)
    singleeventdict.clear()
    c += 1
print(c)

dictofjson = {}
for c in range(0, len(dictofevents)):
    dump_str = json.dumps(dictofevents[c])
    loaded = json.loads(dump_str)
    dictofjson[c] = loaded

print(dictofjson[c])
















# tables = pd.read_html(WEB_PAGE)
#
# pullmanTable = []
# vancouverTable = []
# spokaneTable = []
# tricitiesTable = []
# seattleTable = []
#
# index = 0
# for element in tables[0][3]:
#
#     if element.find("Vancouver") > 0:
#         vancouverTable.append(tables[0][2][index])
#     if element.find("Kennewick") > 0 or element.find("Pasco") > 0 or element.find("Richland") > 0:
#         tricitiesTable.append(tables[0][2][index])
#     if element.find("Spokane") > 0:
#         spokaneTable.append(tables[0][2][index])
#     if element.find("Seattle") > 0:
#         seattleTable.append(tables[0][2][index])
#     index += 1
#
# print("Pullman opprotunities:")
# print(pullmanTable)
# print("Vancouver opprotunities:")
# print(vancouverTable)
# print("TriCities opprotunities:")
# print(tricitiesTable)
# print("Spokane opprotunities:")
# print(spokaneTable)
# print("Seattle opprotunities:")
# print(seattleTable)