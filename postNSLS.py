import pandas as pd
import httplib2
from bs4 import BeautifulSoup, SoupStrainer
from lxml import html
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import requests
import os
import json

WEB_PAGE = "https://members.nsls.org/node/add/event"
login_url = "https://members.nsls.org/memberinfo/area"
test_url = "https://members.nsls.org/chapter-leaders/dashboard"
namevalue = "christopher.l.rober@wsu.edu"
passvalue = "badpass"
token = "form-UnuVOdMukrMGA2ucxR_ajRLGnMx12rDp2IOgwYBQTBU"
payload = {
    "name": "<USER NAME>",
    "pass": "<PASSWORD>",
    "form_build_id": "form-UnuVOdMukrMGA2ucxR_ajRLGnMx12rDp2IOgwYBQTBU",
    "form_id": "user_login_block",
    "op": "Log+in"
}
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"
}

session_request = requests.session()

result = session_request.get(WEB_PAGE, headers=headers)

soup = BeautifulSoup(result.content, 'html.parser')
payload['form_build_id'] = soup.find('input', attrs={'name': 'form_build_id'})['value']


tree = html.fromstring(result.text)

payload['name'] = "christopher.l.rober@wsu.edu"
payload['pass'] = "4hojpfi$"
user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"

print(payload)

result = session_request.post(
    login_url,
    data=payload
)

print(result.status_code, result.reason)

r = session_request.get(WEB_PAGE)

print(r.status_code, r.reason)

postheaders = {
    "Host": "members.nsls.org",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "Referer": "https://members.nsls.org/node/add/event",
    "Content-Type": "multipart/form-data; boundary=---------------------------9562196928923306521866006352",
    "Content-Length": "10149",
    "Connection": "keep-alive",
    "Cookie": "SESS1f94c1e5b0644c5250116d6d24b8eb88=Hij_c2uuJs75TsffpO9hGDDYZMNAr59JsYqOocxB4Bw; SSESS1f94c1e5b0644c5250116d6d24b8eb88=y9id60sJYJ6530tM-lzQRtzsRGDKQP9uB0TKpK7Z1SE; SSESS60abc8dd79fd03c3070def08e0a2b709=pPvWLdKIk8vXlFAr_HROIwgmlOp_LB39rHcuFZ8J5QU; SESS60abc8dd79fd03c3070def08e0a2b709=cugnuPeWxR77_1VgU-nCJHmQeIQZLNebSDAhsX7YYuM; nsls_prod=eJyVU02PmzAQ%2FSvIe41Dvoiy3KJWPW1VtVUPVVlFBgbwytjUNomiFf%2B9Y4cEaMWhlxB73nvMvHm8k5bnJCa73WYdRXuyIJk%2F76OM7Yp8Qw%2Bwfaa7YpvTQ7Tf0XyFSMjS6HBw4BpkS%2BJ3YirVuOcFeFlZEq9XC1JpKFDJlxbEcisAj9%2FdscPXKGlZZsekzUBiqWptEt5BA%2F3D7Sb4YZxIwX6PBbb%2FCDjAQP50%2FOpYjVYFF0Dvzd%2Fpu4H%2BNH5lpbmxqqlAB99UCtoarPKala5aWduYOAmTsIYai2YpjTBLpcskNNyCSUImRBLaCmp3cFW8MgYsPnld4m%2BDNCWXjSydsIXanJi1mqct8l2HmUACiX%2BRXKsmVxfpe6fa9%2F3a9SSHFKrEwWdMaQ3oJOwhw4Avqgy%2B4I3bSsUaC5oKYDk2RXNmqlQxnY8Vo4fgX%2FAkHPBj%2FzwoePGg4OMD0rn8eM8o08Bm8tNDRoKfbzfB0ZGcyJWyLFOtnEvTTQJ9vi1%2BLHUNjj2161DqCX1nWQV%2B3jdv%2BXiJtcpb4U5Zi4moccMq42Cvp96I08OIB3IGcXo4lYRvLhbK8oJnzHIlzRLf7LY6uZzsdI0rx48DJygYTUEIqnCqmdD4rEhFM3QZ47IgObOM%2BqlBk3jVTWKfstwFW7ZC%2FH8YA%2F835eU0lTgIRQOnG17Nx2gyuPceG6tsjQpWtzDs76dqg4qdIZAqkHAJpo7NOnLmcPENvXZ%2B7TXjk%2Ba2Q8R9aew2yDMI1YB3vDcrOqDMH6GOzIQ%3D; nsls_prod_exp=1554434141; nsls_prod_env_prod=1; has_js=1",
    "Upgrade-Insecure-Requests": "1"
}

location = "north pole"
rsvplimit = "69"
objecttosend = {
    "title": "testChristmas",
    "field_event_date[und][0][value][date]": "Dec 25 2019",
    "field_event_additional_info[und][0][value]": "ChristmasTreeUrl",
    "field_event_type[und]": "27971",
    "field_event_location[und][0][value]": "north pole",
    "field_event_rsvp_limit[und][0][value]": "69",
    "field_contact_person[und]": "4421556",
    "field_event_date[und][0][all_day]": "1",
    "field_event_location_address[und][0][locality]": "North Pole City"
}

postr = session_request.post(
    WEB_PAGE,
    # headers=postheaders,
    data=objecttosend
)










# driver = webdriver.Firefox()
# driver.get(WEB_PAGE)
#
# u = driver.find_element_by_name('name')
# u.send_keys('christopher.l.rober@wsu.edu')
# p = driver.find_element_by_name('pass')
# p.send_keys('4hojpfi$')
# p.send_keys(Keys.RETURN)
#
# result = session_request.get(
#     test_url,
#     headers = dict(referer = test_url)
# )
# print(result)
# print(result.ok)
# tree = html.fromstring(result.content)
# speaker_names = tree.xpath("//div[@h5]/a/text()")
#
# print(speaker_names)